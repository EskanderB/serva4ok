import requests
import sys
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5 import *
import json
from main import Ui_MainWindow


def open_registration_window():
    from registration import Mswindows


class Mswindows(QtWidgets.QMainWindow):

    def __init__(self):
        super(Mswindows, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.ui.lineEdit_3.clicked.connect(self.change_text_line)
        self.ui.lineEdit_2.clicked.connect(self.change_text_line_2)

        self.ui.pushButton_3.clicked.connect(open_registration_window)
        self.ui.pushButton.clicked.connect(self.account_login)

    def change_text_line(self):
        self.ui.lineEdit_3.clear()
        self.ui.lineEdit_3.setStyleSheet("background-color:white")

    def change_text_line_2(self):
        self.ui.lineEdit_2.clear()
        self.ui.lineEdit_2.setStyleSheet("background-color:white")

    def account_login(self):
        b = self.ui.lineEdit_3.text()
        c = self.ui.lineEdit_2.text()
        a = requests.get(('http://127.0.0.1:5000/in/' + b + '/' + c))

        self.ui.lineEdit_3.clear()
        self.ui.lineEdit_2.clear()
        if str(a.status_code) in "200":
            print(str(a.status_code) in "200", "FG")
            from account import MyWindows
            self.ui.label.clear()
        else:
            self.ui.label.setText("Вы ввели неправильный логин или пароль")

    def my_exception_hook(exctype, value, traceback):
        # Print the error and traceback
        print(exctype, value, traceback)
        # Call the normal Exception hook after
        sys._excepthook(exctype, value, traceback)
        sys.exit(1)

    # Back up the reference to the exceptionhook
    sys._excepthook = sys.excepthook

    # Set the exception hook to our wrapping function
    sys.excepthook = my_exception_hook


app = QtWidgets.QApplication([])
application = Mswindows()
application.show()
sys.exit(app.exec())
